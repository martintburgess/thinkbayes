thinkbayes
===========
I'm working through the text book "[Think Bayes]" by Allen Downey. Allen is kind enough to provide a PDF of the book for free. 

The code is a clone of the the Allen Downey's [ThinkBayes2] repo.Text and code for the second edition of Think Bayes, by Allen Downey.

[Think Bayes]: https://greenteapress.com/wp/think-bayes/
[ThinkBayes2]: https://github.com/AllenDowney/ThinkBayes2
